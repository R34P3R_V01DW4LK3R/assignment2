//
//  main.swift
//  refactoring
//
//  Created by NICODEMUS CHAN on 2/6/16.
//  Copyright © 2016 Institute of Technical Education. All rights reserved.
//

import Foundation


enum PriceCode {
    case REGULAR
    case NEW_RELEASE
    case CHILDRENS
    
    func getPrice() -> Price{
        switch(self) {
        case .REGULAR:
            return RegularPrice()
        case .NEW_RELEASE:
            return NewReleasePrice()
        case .CHILDRENS:
            return ChildrenPrice()
        
            
        }
        
    }
}

protocol Price {
var priceCode:PriceCode{ get }

func getCharge(daysRented: Int ) ->Double
}

class RegularPrice:Price{
    var priceCode:PriceCode = .REGULAR
    
    func getCharge(daysRented: Int) -> Double {
        var thisAmount = 2.0
        if daysRented > 2 {
            thisAmount += (Double(daysRented - 2) * 1.5)
        }
            return thisAmount
    }
}

class NewReleasePrice : Price {
    var priceCode: PriceCode = .NEW_RELEASE
    
    func getCharge(daysRented: Int) -> Double {
        return Double (daysRented * 3)
    }
}

class ChildrenPrice : Price {
    var priceCode: PriceCode = .CHILDRENS
    
    func getCharge(daysRented: Int) -> Double {
        var thisAmount = 1.5
        if  daysRented > 3 {
            thisAmount += Double(daysRented - 3) * 1.5
        }
        return thisAmount
    }

}

class Movie {
    
    let title:String
    let priceCode:PriceCode
    
    init(title: String, priceCode: PriceCode) {
        self.title = title
        self.priceCode = priceCode
    }
    private func getCharge(daysRented: Int) -> Double {
        /*var result = 0.0
        
        switch(priceCode) {
        case .REGULAR:
            result += 2
            if daysRented > 2 {
                result += (Double(daysRented - 2) * 1.5)
            }
        case .NEW_RELEASE:
            result += Double(daysRented * 3)
            
        case .CHILDRENS:
            result += 1.5
            if daysRented > 3 {
                result += Double(daysRented - 3) * 1.5
            }
        }*/
        return priceCode.getPrice().getCharge(daysRented)
        
       
    }
    
}

class Rental {
    let movie:Movie
    let daysRented:Int
    
    init(movie: Movie, daysRented:Int) {
        self.movie = movie
        self.daysRented = daysRented
        
    }
    func getCharge() -> Double {
        return movie.getCharge(daysRented)
    }
    
    
    func getFrequentRenterPoints() -> Int {
        // Add bonus for a two day new release rental
        if movie.priceCode == .NEW_RELEASE && daysRented > 1 {
            return 2
        }
          return 1
    }
    
}

class Customer {
    let name:String
    private var rentals = [Rental]()
    
    init(name: String) {
        self.name = name
    }
    
    func addRental(arg: Rental) {
        rentals.append(arg)
    }
    
    func statement() -> String {
        //var totalAmount = 0.0
        
        
        var result = "Rental record for \(self.name)\n"
        
        for rental in rentals {
            //let thisAmount = rental.getCharge()
            
          
         
            
            
            result += "\t\(rental.movie.title)\t\(rental.getCharge())\n"
            
            //totalAmount += rental.getCharge()
            
        }
        
        // Add footer lines
        result += "Amount owed is \(getTotalCharge())\n"
        result += "You earned \(getTotalFrequentRenterPoints()) frequent renter points"
        
        return result
        

        
    }
    func htmlStatement() ->String {
        var result = "<h1>Rental record for <em>\(self.name)</em></h1>\n"
        
        result += "<p>"
        
        for rental in rentals {
            //let thisAmount = rental.getCharge()
            
            result += "\t\(rental.movie.title)\t\(rental.getCharge())\n"
            
            //totalAmount += rental.getCharge()
            
        }
        result += "</p>"
        
        // Add footer lines
        result += "<p>Amount owed is <em>\(getTotalCharge())</em></p>\n"
        result += "<p>You earned <em>\(getTotalFrequentRenterPoints())</em> frequent renter points</p>"
        
        return result

    }
    
    private func getTotalCharge() ->Double{
        var result = 0.0
        
        for rental in rentals {
            result += rental.getCharge()
        }
        return result
    }
    
    private func getTotalFrequentRenterPoints() ->Int{
    var result = 0
    
    for rental in rentals {
    result += rental.getFrequentRenterPoints()
    }
    return result
    }

    
    /*private func amountFor(rental: Rental) -> Double {
        var result = 0.0
        
        switch(rental.movie.priceCode) {
        case .REGULAR:
            result += 2
            if rental.daysRented > 2 {
                result += (Double(rental.daysRented - 2) * 1.5)
            }
        case .NEW_RELEASE:
            result += Double(rental.daysRented * 3)
            
        case .CHILDRENS:
            result += 1.5
            if rental.daysRented > 3 {
                result += Double(rental.daysRented - 3) * 1.5
            }
        }
        
        
        return result
    }*/
}
